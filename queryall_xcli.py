import xml.etree.cElementTree as ET 
import os


input_directory = os.path.abspath(os.path.join('data','input'))
output_directory = os.path.abspath(os.path.join('data','output'))


def main():
    # add_root(directory, 'root')
    for file_name in os.listdir(input_directory):
        full_file = os.path.join(input_directory, file_name)
        output_file = os.path.join(output_directory, file_name.split('.')[0]) + '.txt'
        f = open(output_file, 'w')
        tree = ET.ElementTree(file=full_file)
        root = tree.getroot()

        command_list = create_command_list(root)
        fc_port_dict = xml_to_dict_list(root, 'fc_port_list -x')
        host_connectivity_dict = xml_to_dict_list(root, 'host_connectivity_list -x')
        module_dict = xml_to_dict_list(root, 'module_list -x')
        cluster_dict = xml_to_dict_list(root, 'cluster_list -x')
        host_dict = xml_to_dict_list(root, 'host_list -x')
        ipinterface_dict = xml_to_dict_list(root, 'ipinterface_list -x')
        target_dict = xml_to_dict_list(root, 'target_list -x')
        pool_dict = xml_to_dict_list(root, 'pool_list -x')
        config_dict = xml_to_dict_list(root, 'config_get -x')
        state_dict = xml_to_dict_list(root, 'state_list -x')
        vol_dict = xml_to_dict_list(root, 'vol_list -x')

        mt = get_machinetype(config_dict)
        
        list_config(config_dict, f)
        list_ipinterfaces(ipinterface_dict, f)
        list_modules(module_dict, f)
        list_pools(pool_dict, f, mt)
        list_fc_ports(fc_port_dict, f)
        list_targets(target_dict, f)
        


def get_machinetype(this_dict):
    for item in this_dict:
        if item['name'] == 'machine_type':
            mt = item['value']
    return mt



def add_root(input_directory, root_tag):
    for file_name in os.listdir(input_directory):
        full_file = os.path.join(input_directory, file_name)
        with open(full_file, 'r') as f:
            data = f.read()
        with open(full_file, 'w') as g:
            g.write(f'<{root_tag}>\n{data}\n</{root_tag}>')
            g.close()


def list_config(this_dict, f):
    print('System Parameters', file=f)
    for item in this_dict:
        if item['name'] == 'system_name':
            print('    System Name: ' + item['value'], file=f)
        if item['name'] == 'machine_type':
            print('    Machine Type: ' + item['value'], file=f)
        if item['name'] == 'machine_model':
            print('    Model: ' + item['value'], file=f)
        if item['name'] == 'machine_serial_number':
            print('    Serial Number: ' + item['value'], file=f)


def list_pools(this_dict, f, mt):
    print('Pools', file=f)
    for item in this_dict:
        if mt == '9837':
            print('    ' + item['name'], file=f)
            print(f'        Size:  {item["size_MiB"]} MiB', file=f)
            print(f'        Used by volumes: {item["used_by_volumes_MiB"]} MiB', file=f)
            print(f'        Free space: {item["empty_space_MiB"]} MiB', file=f)
        elif mt == '2812':
            print('    ' + item['name'], file=f)
            print(f'        Soft size:  {item["soft_size_MiB"]} MiB', file=f)
            print(f'        Used by volumes: {item["used_by_volumes_MiB"]} MiB', file=f)
            print(f'        Free space: {item["empty_space_soft_MiB"]} MiB', file=f)


def list_targets(this_dict, f):
    print('Target Systems', file=f)
    for item in this_dict:
        if item:
            print('    ' + item['name'], file=f)
            print('        Ports', file=f)
            ports = item['port_list'].split(',')
            for port in ports:
                print('            ' + port, file=f)
            print(f'        Max Init Rate: {item["max_initialization_rate"]}', file=f)
            print(f'        Max Resync Rate: {item["max_resync_rate"]}', file=f)
            print(f'        Max Sync Job Rate: {item["max_syncjob_rate"]}', file=f)
        else:
            print('    NO REMOTE TARGETS DEFINED', file=f)

def list_ipinterfaces(this_dict, f):
    print('Management IPs', file=f)
    for item in this_dict:
        if item['type'] == 'Management':
            print('    IP Address: ' + item['address'], file=f)
            print('    Subnet Mask: ' + item['netmask'], file=f)
            print('    Default Gateway: ' + item['gateway'], file=f)
            print(file=f)


def list_modules(this_dict, f):
    print('Installed Modules:', file=f)
    for item in this_dict:
        for key, value in item.items():
            if key == 'component_id':
                splits = value.split(':')
                print(f'    Module: {splits[2]}', file=f)


def list_fc_ports(this_dict, f):
    print('Fibre Channel Ports:', file=f)
    for item in this_dict:
        for key, value in item.items():
            if key == 'component_id':
                splits = value.split(':')
                print(f'    Module: {splits[2]}, Port: {splits[3]}', file=f)
            if key == 'role':
                print('        Role: ' + value, file=f)
            if key == 'port_state':
                print('        State: ' + value, file=f)
            if key == 'current_rate':
                if value == '0':
                    print(f'        Speed:  --', file=f)
                else:
                    print(f'        Speed: {value} Gbps', file=f)
            if key == 'wwpn':
                print('        WWPN: ' + value, file=f)


def xml_to_dict_list(root, this_command):
    command_dict_list = []
    for xclireturn in root:
        command = xclireturn.get('COMMAND_LINE')
        if command == this_command:
            for output in xclireturn:
                for item in output:
                    item_dict = {}
                    for attr in item:
                        item_dict[attr.tag] = attr.get('value')
                    command_dict_list.append(item_dict)
    return command_dict_list


def create_command_list(root):
    command_list = []
    for xclireturn in root:
        command = xclireturn.get('COMMAND_LINE')
        command_list.append(command)
    return command_list


def iterate_dict(command_dict):
    for key, value in command_dict.items():
        print(key)
        for line in value:
            for k, v in line.items():
                print(f'{k}: {v}')


def iterate_list(command_list):
    for command in command_list:
        print(command)


if __name__ == '__main__':
    main()

    
